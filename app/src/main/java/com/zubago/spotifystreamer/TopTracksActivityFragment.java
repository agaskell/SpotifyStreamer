package com.zubago.spotifystreamer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.zubago.spotifystreamer.models.Track;

import java.util.ArrayList;
import java.util.HashMap;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.Tracks;

/**
 * A placeholder fragment containing a simple view.
 */
public class TopTracksActivityFragment extends Fragment {
    TrackArrayAdapter mTrackAdapter;
    ListView mTrackListView;
    TextView mNoTopTracksView;
    ArrayList<Track> mTracks;
    String mArtist;

    static final String TRACKS = "TRACKS";

    public TopTracksActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTracks = new ArrayList<>();

        if(savedInstanceState != null) {
            mTracks = savedInstanceState.getParcelableArrayList(TRACKS);
            mArtist = savedInstanceState.getString(Constants.ARTIST_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mTrackAdapter = new TrackArrayAdapter(getActivity(),
                R.layout.list_item_track, R.id.list_item_track_textview, mTracks);

        Intent intent = getActivity().getIntent();
        View view = inflater.inflate(R.layout.fragment_top_tracks, container, false);

        mNoTopTracksView = (TextView)view.findViewById(R.id.no_top_tracks_textview);
        mTrackListView = (ListView)view.findViewById(R.id.top_tracks_listview);
        mTrackListView.setAdapter(mTrackAdapter);

        if(savedInstanceState == null && intent != null && intent.hasExtra(Constants.ARTIST_ID)) {
            mArtist = intent.getStringExtra(Constants.ARTIST_NAME);
            (new TopTracksTask()).execute(intent.getStringExtra(Constants.ARTIST_ID));
        } else {
            mTrackListView.setEmptyView(mNoTopTracksView);
        }

        mNoTopTracksView.setText(mArtist + getString(R.string.no_top_tracks));
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(TRACKS, mTracks);
        outState.putString(Constants.ARTIST_NAME, mArtist);
        super.onSaveInstanceState(outState);
    }

    public class TopTracksTask extends AsyncTask<String, Void, ArrayList<Track>> {
        private final String LOG_TAG = TopTracksTask.class.getSimpleName();

        @Override
        protected ArrayList<Track> doInBackground(String... params) {
            HashMap<String, Object> options = new HashMap<String, Object>() {
                { put("country", "US"); }
            };
            try {
                SpotifyService service = (new SpotifyApi()).getService();
                Tracks serviceTracks = service.getArtistTopTrack(params[0], options);

                ArrayList<Track> tracks = new ArrayList<>(serviceTracks.tracks.size());
                for(kaaes.spotify.webapi.android.models.Track t : serviceTracks.tracks) {
                    tracks.add(new Track(t.name, t.album.name,
                            t.album.images.size() > 0 ? t.album.images.get(0).url : null));
                }

                return tracks;
            } catch (Exception e) {
                Log.e(LOG_TAG, e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Track> tracks) {
            mTracks = tracks;
            mTrackAdapter.clear();
            mTrackListView.setEmptyView(mNoTopTracksView);

            if(tracks != null) {
                mTrackAdapter.addAll(tracks);
            }
        }
    }
}
