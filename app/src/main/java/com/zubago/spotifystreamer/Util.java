package com.zubago.spotifystreamer;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

public class Util {
    private static int mListViewImagePixels = -1;

    public static int devicePixelsToPixels(Context context, int devicePixels) {
        return (int) Math.ceil(devicePixels * context.getResources().getDisplayMetrics().density);
    }

    public static int listViewThumbnailSize(Context context) {
        if(mListViewImagePixels == -1) {
            mListViewImagePixels = devicePixelsToPixels(context, 50);
        }
        return mListViewImagePixels;
    }

    public static void setTextView(View v, String value, int id) {
        ((TextView)v.findViewById(id)).setText(value);
    }
}
