package com.zubago.spotifystreamer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.zubago.spotifystreamer.models.Artist;

import java.util.ArrayList;

import kaaes.spotify.webapi.android.SpotifyApi;
import kaaes.spotify.webapi.android.SpotifyService;
import kaaes.spotify.webapi.android.models.ArtistsPager;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    SpotifyService mService;
    ArtistArrayAdapter mArtistAdapter;
    TextView mNoResultsTextView;
    EditText mSearchText;
    String mSearchTerm;

    ListView mArtistListView;
    SearchArtistTask mSearchArtistTask;
    ArrayList<Artist> mArtists;

    static final String ARTISTS = "ARTISTS";
    static final String SEARCH_TERM = "SEARCH_TERM";

    public MainActivityFragment() {
        mService = (new SpotifyApi()).getService();
        mSearchArtistTask = new SearchArtistTask();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mArtists = new ArrayList<>();

        if(savedInstanceState != null) {
            mArtists = savedInstanceState.getParcelableArrayList(ARTISTS);
            mSearchTerm = savedInstanceState.getString(SEARCH_TERM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mArtistAdapter = new ArtistArrayAdapter(getActivity(),
                R.layout.list_item_artist, R.id.list_item_artist_textview, mArtists);
        
        final View view = inflater.inflate(R.layout.fragment_main, container, false);

        mNoResultsTextView = (TextView)view.findViewById(R.id.no_results_textview);

        mArtistListView = (ListView)view.findViewById(R.id.results_listview);
        mArtistListView.setEmptyView(mNoResultsTextView);
        mArtistListView.setAdapter(mArtistAdapter);

        mArtistListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Artist artist = mArtistAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), TopTracksActivity.class)
                        .putExtra(Constants.ARTIST_ID, artist.id)
                        .putExtra(Constants.ARTIST_NAME, artist.name);
                startActivity(intent);
            }
        });

        mSearchText = (EditText) view.findViewById(R.id.search_text);
        mSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().equals(mSearchTerm)) { return; }

                if (mSearchArtistTask.getStatus() != AsyncTask.Status.FINISHED) {
                    mSearchArtistTask.cancel(true);
                }

                if (s.length() == 0) {
                    mArtistAdapter.clear();
                    mNoResultsTextView.setText("");
                    return;
                }

                mSearchArtistTask = new SearchArtistTask();
                mSearchArtistTask.execute(s.toString());
            }

            @Override
            public void afterTextChanged(final Editable s) {

            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(ARTISTS, mArtists);
        outState.putString(SEARCH_TERM, mSearchTerm);
        super.onSaveInstanceState(outState);
    }

    public class SearchArtistTask extends AsyncTask<String, Void, ArrayList<Artist>> {
        private final String LOG_TAG = SearchArtistTask.class.getSimpleName();

        @Override
        protected ArrayList<Artist> doInBackground(String... params) {
            mSearchTerm = params[0];
            try {
                ArtistsPager artistsPager = mService.searchArtists(mSearchTerm);
                ArrayList<Artist> artists = new ArrayList<>(artistsPager.artists.items.size());
                for(kaaes.spotify.webapi.android.models.Artist a : artistsPager.artists.items) {
                    artists.add(new Artist(a.id, a.name,
                            a.images.size() > 0 ? a.images.get(0).url : null));
                }

                return artists;
            } catch (Exception e) {
                Log.e(LOG_TAG, e.toString());
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Artist> artists) {
            mArtists = artists;
            mArtistAdapter.clear();
            mNoResultsTextView.setText(getString(R.string.coundnt_find_artist) + mSearchTerm +
                    getString(R.string.bummer));

            if(artists != null) {
                mArtistAdapter.addAll(artists);
            }
        }
    }

}
