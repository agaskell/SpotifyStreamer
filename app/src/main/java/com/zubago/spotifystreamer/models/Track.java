package com.zubago.spotifystreamer.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Track implements Parcelable {
    public String name;
    public String albumName;
    public String albumImage;

    public Track(String name, String albumName, String albumImage) {
        this.name = name;
        this.albumName = albumName;
        this.albumImage = albumImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeString(name);
        p.writeString(albumName);
        p.writeString(albumImage);
    }

    public static final Creator<Track> CREATOR
            = new Creator<Track>() {
        public Track createFromParcel(Parcel in) {
            return new Track(in);
        }

        public Track[] newArray(int size) {
            return new Track[size];
        }
    };

    private Track(Parcel p) {
        name = p.readString();
        albumName = p.readString();
        albumImage = p.readString();
    }
}
