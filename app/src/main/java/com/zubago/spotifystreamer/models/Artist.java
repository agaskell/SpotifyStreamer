package com.zubago.spotifystreamer.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Artist implements Parcelable {
    public String id;
    public String name;
    public String image;

    public Artist(String id, String name, String image)  {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeString(id);
        p.writeString(name);
        p.writeString(image);
    }

    public static final Creator<Artist> CREATOR
            = new Creator<Artist>() {
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    private Artist(Parcel p) {
        id = p.readString();
        name = p.readString();
        image = p.readString();
    }
}
