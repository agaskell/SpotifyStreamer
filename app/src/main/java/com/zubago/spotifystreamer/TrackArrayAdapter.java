package com.zubago.spotifystreamer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zubago.spotifystreamer.models.Track;

import java.util.List;


public class TrackArrayAdapter extends ArrayAdapter<Track> {
    private int mImageSideLength;

    public TrackArrayAdapter(Context context, int resource, int textViewResourceId, List<Track> tracks) {
        super(context, resource, textViewResourceId, tracks);
        mImageSideLength = Util.listViewThumbnailSize(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // please review this line. I have seen code that manually inflates the view
        // instead of calling super. I'd like to know which way is preferred.
        // See CustomListAdapter.java from http://www.androidinterview.com/android-custom-listview-with-image-and-text-using-arrayadapter/
        View view = super.getView(position, convertView, parent);

        Track track = getItem(position);
        Util.setTextView(view, track.name, R.id.list_item_track_textview);
        Util.setTextView(view, track.albumName, R.id.list_item_album_textview);

        if(track.albumImage != null) {
            ImageView imageView = (ImageView)view.findViewById(R.id.list_item_album_image);
            Picasso.with(getContext())
                    .load(track.albumImage)
                    .resize(mImageSideLength, mImageSideLength)
                    .centerCrop()
                    .into(imageView);
        }
        return view;
    }
}
