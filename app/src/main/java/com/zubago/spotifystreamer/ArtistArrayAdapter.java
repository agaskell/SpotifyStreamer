package com.zubago.spotifystreamer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zubago.spotifystreamer.models.Artist;

import java.util.List;


public class ArtistArrayAdapter extends ArrayAdapter<Artist> {
    private int mImageSideLength;

    public ArtistArrayAdapter(Context context, int resource, int textViewResourceId,
                              List<Artist> artists) {
        super(context, resource, textViewResourceId, artists);

        // match image size to the layout
        mImageSideLength = Util.listViewThumbnailSize(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // please review this line. I have seen code that manually inflates the view
        // instead of calling super. I'd like to know which way is preferred.
        // See CustomListAdapter.java from http://www.androidinterview.com/android-custom-listview-with-image-and-text-using-arrayadapter/
        View view = super.getView(position, convertView, parent);

        Artist artist = getItem(position);
        Util.setTextView(view, artist.name, R.id.list_item_artist_textview);

        if(artist.image != null) {
            ImageView imageView = (ImageView)view.findViewById(R.id.list_item_artist_image);
            Picasso.with(getContext())
                    .load(artist.image)
                    .resize(mImageSideLength, mImageSideLength)
                    .centerCrop()
                    .into(imageView);
        }
        return view;
    }
}
